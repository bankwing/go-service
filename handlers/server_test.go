package server

import (
	"net/http"
	"net/http/httptest"
	"testing"
	"github.com/labstack/echo"
	"github.com/stretchr/testify/assert"
)

func TestRootUrl(t *testing.T) {
	e := echo.New()
	req := httptest.NewRequest(echo.GET, "/", nil)
	rec := httptest.NewRecorder()
	// Using the ServerHTTP on echo will trigger the router and middleware
	//c := e.ServeHTTP(rec, req)
        c := e.NewContext(req, rec)
        c.SetPath("/")

       if assert.NoError(t, RootUrl(c)) {
         assert.Equal(t, http.StatusOK, rec.Code)
         assert.Contains(t, rec.Body.String(), "Hello World!!")
       }

}

func TestHealthUrl(t *testing.T) {
        e := echo.New()
        req := httptest.NewRequest(echo.GET, "/", nil)
        rec := httptest.NewRecorder()
        // Using the ServerHTTP on echo will trigger the router and middleware
        //c := e.ServeHTTP(rec, req)
        c := e.NewContext(req, rec)
        c.SetPath("/custinfo")
        
        var healthJSON = `{"alive":true}`

       if assert.NoError(t, HealthUrl(c)) {
         assert.Equal(t, http.StatusOK, rec.Code)
         assert.Equal(t, healthJSON, rec.Body.String())
       }

}

