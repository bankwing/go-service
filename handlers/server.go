package server

import (
	"net/http"
//        "encoding/json"
	"fmt"
        "io/ioutil"
        "log"
        "os"
	"github.com/labstack/echo"
)

func getEnv(key, fallback string) string {
    if value, ok := os.LookupEnv(key); ok {
        return value
    }
    return fallback
}

func RootUrl(c echo.Context) error {
	return c.HTML(http.StatusOK, "Hello World!!")
}

func HealthUrl(c echo.Context) error {
        var healthJSON = `{"alive":true}`
	return c.HTML(http.StatusOK, healthJSON)
}

func Service2Url(c echo.Context) error {
        var host = getEnv("HOST","127.0.0.1")
        var port = getEnv("PORT","8081")

        // Build the request

                response, err := http.Get("http://"+host+":"+port+"/version")

                if err != nil {
                  fmt.Print(err.Error())
                  os.Exit(1)
                }

                responseData, err := ioutil.ReadAll(response.Body)
                if err != nil {
                  log.Fatal(err)
                }
                fmt.Println(string(responseData))
                var res = "{response:"+string(responseData)+"}"
                return c.String(http.StatusOK, res )

}

