############################
# STEP 1 build executable binary
############################
FROM prod1.ktbcs.co.th:8500/poc/golang:alpine AS builder

WORKDIR /xxx
COPY . .

# Build the binary.
RUN GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -o /xxx/app
############################
# STEP 2 build a small image
############################
FROM alpine:3.8
# Copy our static executable.
COPY --from=builder /xxx/app /go/bin/app

ENTRYPOINT ["/go/bin/app"]
