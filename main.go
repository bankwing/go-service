package main

import (
	"github.com/labstack/echo"
        "github.com/labstack/echo/middleware"
	"gitlab.com/bankwing/go-service/handlers"
)

func main() {
	e := echo.New()
        e.Use(middleware.Logger())

	e.GET("/", server.RootUrl)
	e.GET("/health", server.HealthUrl)
	e.GET("/service2", server.Service2Url)

	e.Logger.Fatal(e.Start(":1323"))
}

